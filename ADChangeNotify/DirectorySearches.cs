﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This is an unrelated class to make queries to AD for users. Thought it'd be nice to query really quick if I needed any additional info that could be useful.
//Not required for monitoring.
namespace ADChangeNotify
{
    public class DirectorySearches
    {
        public void GetAllUsers()
        {
            SearchResultCollection results;
            DirectorySearcher ds = null;
            DirectoryEntry de = new
            DirectoryEntry(GetCurrentDomainPath());

            ds = new DirectorySearcher(de);
            ds.Filter = "(&(objectCategory=User)(objectClass=person))";

            results = ds.FindAll();

            foreach (SearchResult sr in results)
            {
                // Using the index zero (0) is required!
                Debug.WriteLine(sr.Properties["name"][0].ToString());
            }
        }

        public void GetAUser(string userName)
        {
            DirectorySearcher ds = null;
            DirectoryEntry de = new DirectoryEntry(GetCurrentDomainPath());
            Console.WriteLine(de.Path);
            SearchResult sr = null;

            // Build User Searcher
            ds = BuildUserSearcher(de);
            // Set the filter to look for a specific user
            ds.Filter = "(&(objectCategory=User)(objectClass=person)(sAMAccountName=" + userName + "))";
   
            try { 
                
                sr = ds.FindOne();
                if (sr != null)
                {
                    ResultPropertyCollection myResultPropColl;
                    myResultPropColl = sr.Properties;
                    Console.WriteLine("The properties of the " +
                            "'mySearchResult' are :");

                    foreach (string myKey in myResultPropColl.PropertyNames)
                    {
                        string tab = "    ";
                        Console.WriteLine(myKey + " = ");
                        foreach (Object myCollection in myResultPropColl[myKey])
                        {
                            Console.WriteLine(tab + myCollection);
                        }
                    }
                    ds.Dispose();
                    de.Dispose();



                    Console.WriteLine(sr);
                    /*/ Debug.WriteLine(sr.GetPropertyValue("mail"));
                     Debug.WriteLine(sr.GetPropertyValue("givenname"));
                     Debug.WriteLine(sr.GetPropertyValue("sn"));
                     Debug.WriteLine(sr.GetPropertyValue("userPrincipalName"));
                     Debug.WriteLine(sr.GetPropertyValue("distinguishedName"));*/
                }
                else
                {
                    Console.WriteLine("not found");
                }

            }
            catch(Exception e) {
                Console.WriteLine(e.Message);
            }
            

            
        }

        private string GetCurrentDomainPath()
        {
            DirectoryEntry de = new DirectoryEntry("LDAP://RootDSE");

            return "LDAP://" + de.Properties["defaultNamingContext"][0].ToString();
        }

        public DirectorySearcher BuildUserSearcher(DirectoryEntry de)
        {
            DirectorySearcher ds = null;

            ds = new DirectorySearcher(de);

            // Full Name
            ds.PropertiesToLoad.Add("name");

            // Email Address
            ds.PropertiesToLoad.Add("mail");

            // First Name
            ds.PropertiesToLoad.Add("givenname");

            // Last Name (Surname)
            ds.PropertiesToLoad.Add("sn");

            // Login Name
            ds.PropertiesToLoad.Add("userPrincipalName");

            // Distinguished Name
            ds.PropertiesToLoad.Add("distinguishedName");

            ds.PropertiesToLoad.Add("uSNChanged");

            ds.PropertiesToLoad.Add("mobile");

            return ds;
        }
    }
}
