﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
//This I was using to test when I was testing on my a local VM running a domain controller with AD Services(Off network).
//Keeping it around just in case I need it for reference.
namespace ADChangeNotify
{
    public class ChangeNotifierV2 : IDisposable
    {
        LdapConnection _connection;
        HashSet<IAsyncResult> _results = new HashSet<IAsyncResult>();
        string _dn;
        SearchScope _scope;

        public ChangeNotifierV2(LdapConnection connection, string dn, SearchScope scope)
        {
            _connection = connection;
            SecureString pw = new NetworkCredential("", "Letmein123!").SecurePassword;
            NetworkCredential myCred = new NetworkCredential(
            "tguy", pw, "PETRA.LOCAL");
            _connection.Bind(myCred);
            //_connection.AutoBind = true;
            _dn = dn;
            _scope = scope;
        }
        public void Register()
        {
            try
            {
                SearchRequest request = new SearchRequest(
                _dn, //root the search here
                "(objectClass=*)", //very inclusive
                _scope, //any scope works
                null //we are interested in all attributes
                );

                //register our search
                request.Controls.Add(new DirectoryNotificationControl());

                //we will send this async and register our callback
                //note how we would like to have partial results

                IAsyncResult result = _connection.BeginSendRequest(
                    request,
                    //TimeSpan.FromSeconds(),
                    TimeSpan.FromDays(1), //set timeout to a day...
                    PartialResultProcessing.ReturnPartialResultsAndNotifyCallback,
                    Notify,
                    request);

                //store the hash for disposal later

                _results.Add(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void Notify(IAsyncResult result)
        {
            try
            {
                //since our search is long running, we don't want to use EndSendRequest
                PartialResultsCollection prc = _connection.GetPartialResults(result);

                foreach (SearchResultEntry entry in prc)
                {
                    OnObjectChanged(new ObjectChangedEventArgs(entry));
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Register();
            }

        }

        private void OnObjectChanged(ObjectChangedEventArgs args)
        {
            if (ObjectChanged != null)
            {
                ObjectChanged(this, args);
            }
        }

        public event EventHandler<ObjectChangedEventArgs> ObjectChanged;

        #region IDisposable Members

        public void Dispose()
        {
            foreach (var result in _results)
            {
                //end each async search
                _connection.Abort(result);

            }
        }

        #endregion
    }


    public class ObjectChangedEventArgsV2 : EventArgs
    {
        public ObjectChangedEventArgsV2(SearchResultEntry entry)
        {
            Result = entry;
        }

        public SearchResultEntry Result { get; set; }
    }
}


/*doNotificationStuff();

static void doNotificationStuff()
{
    string domain = "PETRA.LOCAL";
    using (LdapConnection ldapConnection = new LdapConnection(domain))
    {
        using (ChangeNotifier notifier = new ChangeNotifier(ldapConnection, "CN=users,dc=PETRA,dc=LOCAL", SearchScope.Subtree))
        {
            notifier.Register();

            notifier.ObjectChanged += new EventHandler<ObjectChangedEventArgs>(notifier_ObjectChanged);

            Console.WriteLine("Waiting for changes...");
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}


static void notifier_ObjectChanged(object sender, ObjectChangedEventArgs e)
{
    Console.WriteLine(e.Result.DistinguishedName);

    foreach (string attrib in e.Result.Attributes.AttributeNames)
    {
        foreach (var item in e.Result.Attributes[attrib].GetValues(typeof(string)))
        {
            Console.WriteLine("\t{0}: {1}", attrib, item);
        }
    }
    Console.WriteLine();
    Console.WriteLine("====================");
    Console.WriteLine();
}*/