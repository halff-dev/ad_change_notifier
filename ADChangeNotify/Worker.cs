﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADChangeNotify
{
    public class Worker
    {
        static LdapConnection _connection;
        static ChangeNotifier2 _notifier;

        public Worker()
        {
            
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Service is starting");

            string[] attributes = { "distinguishedname", "objectclass", "objectguid" };
            string searchRoot = "OU=d,DC=c,DC=b,DC=a";

            WaitForChange("localhost", searchRoot);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Service is shutting down");
            _notifier.Dispose();
            return Task.CompletedTask;
        }


        private void WaitForChange(string host, string path)
        {
            _connection = CreateConnection(host);
            _notifier = new ChangeNotifier2(_connection);
            _notifier.Register(path, SearchScope.Subtree);

            _notifier.ObjectChanged += new EventHandler<ObjectChangedEventArgs2>(notifier_ObjectChanged);


        }

        void notifier_ObjectChanged(object sender, ObjectChangedEventArgs2 e)
        {
            Console.WriteLine(e.Result.DistinguishedName);
            if (e.Result.Attributes.AttributeNames != null)
                foreach (string attrib in e.Result.Attributes.AttributeNames)
                {
                    foreach (var item in e.Result.Attributes[attrib].GetValues(typeof(string)))
                    {
                        //
                    }
                }

            Console.WriteLine("");
            Console.WriteLine("====================");
            Console.WriteLine("");
        }
        LdapConnection CreateConnection(string server)
        {
            LdapConnection connect = new LdapConnection(server);
            connect.SessionOptions.ProtocolVersion = 3;
            connect.AuthType = AuthType.Negotiate;

            return connect;
        }
    }
    public class ChangeNotifier2 : IDisposable
    {
        LdapConnection _connection;
        HashSet<IAsyncResult> _results = new HashSet<IAsyncResult>();

        public ChangeNotifier2(LdapConnection connection)
        {
            _connection = connection;
            _connection.AutoBind = true;
        }
        public void Register(string dn, SearchScope scope)
        {
            SearchRequest request = new SearchRequest(
                dn, //root the search here
                "(objectClass=*)", //very inclusive
                scope, //any scope works
                null //we are interested in all attributes
                );
            //register our search
            request.Controls.Add(new DirectoryNotificationControl());
            //we will send this async and register our callback
            //note how we would like to have partial results
            IAsyncResult result = _connection.BeginSendRequest(
                request,
                TimeSpan.FromDays(1),
                PartialResultProcessing.ReturnPartialResultsAndNotifyCallback,
                Notify,
                request
                );
            //store the hash for disposal later
            _results.Add(result);
        }
        private void Notify(IAsyncResult result)
        {
            //since our search is long running, we don't want to use EndSendRequest
            try
            {
                PartialResultsCollection prc = _connection.GetPartialResults(result);
                foreach (SearchResultEntry entry in prc)
                {
                    OnObjectChanged(new ObjectChangedEventArgs2(entry));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
        }
        private void OnObjectChanged(ObjectChangedEventArgs2 args)
        {
            ObjectChanged?.Invoke(this, args);
        }
        public event EventHandler<ObjectChangedEventArgs2> ObjectChanged;
        #region IDisposable Members
        public void Dispose()
        {
            foreach (var result in _results)
            {
                _connection.Abort(result);
            }

        }
        #endregion
    }
    public class ObjectChangedEventArgs2 : EventArgs
    {
        public ObjectChangedEventArgs2(SearchResultEntry entry)
        {
            Result = entry;
        }
        public SearchResultEntry Result { get; set; }
    }
}
