﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");


//string conString = "LDAP://DC=HALFF,DC=AD";
using ADChangeNotify;
using System.DirectoryServices.Protocols;

DirectorySearches searches = new DirectorySearches();
//searches.GetAUser("ah4303");
//searches.GetAllUsers();


doNotificationStuff();


static void doNotificationStuff()
{
    // notifier.Register("CN=users,dc=PETRA,dc=LOCAL", SearchScope.Subtree);
    string domain = "HALFF.AD";
    using (LdapConnection ldapConnection = new LdapConnection(domain))
    {
        using (ChangeNotifier notifier = new ChangeNotifier(ldapConnection))
        {
            notifier.Register("OU=Users,OU=VP-Modify-Test,,dc=HALFF,dc=AD", SearchScope.Subtree);
            //OU = Users - Halff,dc = HALFF,dc = AD

            notifier.ObjectChanged += new EventHandler<ObjectChangedEventArgs>(notifier_ObjectChanged);

            Console.WriteLine("Waiting for changes...");
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}


static void notifier_ObjectChanged(object sender, ObjectChangedEventArgs e)
{
    try
    {
        Console.WriteLine(e.Result.DistinguishedName);
       // Console.WriteLine("Home Drive: {0}", e.Result.Attributes["homedrive"].GetValues(typeof(string))[0]);
        //Console.WriteLine("SamAccountName: {0}", e.Result.Attributes["samaccountname"].GetValues(typeof(string))[0]);
        //Console.WriteLine("Phone Number: {0}", e.Result.Attributes["mobile"].GetValues(typeof(string))[0]);
        Console.WriteLine("Title: {0}", e.Result.Attributes["title"].GetValues(typeof(string))[0]);
        //Console.WriteLine("UsnChanged: {0}", e.Result.Attributes["usnchanged"].GetValues(typeof(string))[0]);
    }
    catch(Exception ex)
    {
        Console.WriteLine(ex.Message + " Probably the attribute is empty.");
    }
    
    //e.Result.Attributes.

    /*foreach (string attrib in e.Result.Attributes.AttributeNames)
     {
         foreach (var item in e.Result.Attributes[attrib].GetValues(typeof(string)))
         {
             Console.WriteLine("\t{0}: {1}", attrib, item);
         }
     }*/
    Console.WriteLine();
    Console.WriteLine("====================");
    Console.WriteLine();
}